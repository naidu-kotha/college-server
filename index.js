const express = require("express");
const bcrypt = require("bcrypt");
const cors = require("cors");
const jwt = require("jsonwebtoken");
const bodyParser = require("body-parser");
const nodemailer = require("nodemailer");
require("dotenv").config();

const PORT = process.env.PORT;
const app = express();
app.use(bodyParser.json());
app.use(express.urlencoded({ extended: true }));
app.use(cors());
const client = require("./client");
const { Connection } = require("pg");

const dbConnection = () => {
  try {
    client.connect();
    // console.log("Connected to PostgreSQL server");
  } catch (e) {
    // console.log(e);
    process.exit(1);
  }
};

dbConnection();

// TOKEN AUTHENTICATION
const tokenAuthentication = (req, res, next) => {
  const authHeader = req.headers.authorization;
  // console.log(authHeader);

  if (authHeader === undefined) {
    res.status(401).send("Access denied. No token provided.");
  } else {
    const jwtToken = authHeader.split(" ")[1];
    jwt.verify(jwtToken, "secret_key", (error, payload) => {
      if (error) {
        res.status(401).send(error);
      } else {
        next();
      }
    });
  }
};

// QUIZ AUTHENTICATION
app.post("/quizauthentication/", (req, res) => {
  const { token } = req.body;
  jwt.verify(token, "secret_key", (error, results) => {
    if (error) {
      res.status(401).send(error);
    } else {
      res.status(200).send(results);
    }
  });
});

// TEST
app.get("/test/", (err, res) => {
  client.query("SELECT * FROM user_details", (err, results) => {
    if (err) {
      // console.log(err);
    } else {
      console.log(
        `Connected to user ${results.rows[0].username} from postgreSQL`
      );
      res.send("Connected to postgreSQL Database");
    }
  });
});

// LOGIN
app.post("/login/", (req, res) => {
  const { username, password } = req.body;
  // // console.log(username, password);
  const query = {
    text: "SELECT * FROM user_details WHERE username=$1",
    values: [username],
  };
  // // console.log(query);
  client.query(query, (error, results) => {
    if (error) {
      res.status(400).send(error);
      return;
    }
    if (results.rows.length === 0) {
      res.status(404);
      res.send("User not found");
      return;
    }
    const storedPassword = results.rows[0].password;
    bcrypt.compare(password, storedPassword, (err, result) => {
      if (err) {
        res.status(500).send("An error occurred while verifying password");
        return;
      }
      if (!result) {
        res.status(401).send("Invalid Password");
        return;
      }
      const payload = { username: username };
      const jwtToken = jwt.sign(payload, "secret_key");
      delete results.rows[0].password;
      res.send({ jwtToken, results: results.rows });
      // // console.log(results.rows[0].password);
    });
  });
});

// ADD STUDENT
app.post("/addstudent/", async (req, res) => {
  const { username, fullname, email, dateOfBirth, gender } = req.body;
  const password = "123123";
  const hashedPassword = await bcrypt.hash(password, 10);
  const role = "student";
  const image_url =
    "https://res.cloudinary.com/dhghcct1x/image/upload/v1682517029/img_avatar_drphwo.png";

  const query = {
    text: "INSERT INTO user_details(username, fullname, email, date_of_birth, gender, role, password, image_url) VALUES($1,$2,$3,$4,$5,$6,$7,$8)",
    values: [
      username,
      fullname,
      email,
      dateOfBirth,
      gender,
      role,
      hashedPassword,
      image_url,
    ],
  };

  client.query(query, (error, results) => {
    if (error) {
      res.status(400).send(error);
    } else {
      res.status(200).send(results);
    }
  });
});

// GET STUDENTS
app.post("/getstudents/", (req, res) => {
  const { email, searchText } = req.body;
  // console.log(searchText);
  const role = "student";
  let query = "";

  if (searchText) {
    const trimmedSearchText = searchText.trim();
    query = {
      text: "SELECT * FROM user_details WHERE role=$1 AND fullname ILIKE TRIM($2) ORDER BY id",
      values: [role, `%${trimmedSearchText}%`],
    };
  } else if (email === undefined) {
    query = {
      text: "SELECT * FROM user_details WHERE role=$1 ORDER BY id",
      values: [role],
    };
  } else {
    query = {
      text: "SELECT * FROM user_details WHERE email=$1",
      values: [email],
    };
  }

  client.query(query, (error, results) => {
    if (error) {
      res.status(400).send(error);
    } else {
      const rows = results.rows;
      rows.forEach((row) => {
        delete row.password;
      });
      // console.log(rows);
      res.send(rows);
    }
  });
});

// SEND MAIL
app.post("/sendmail/", (req, res) => {
  const { to, link, testId, testDate } = req.body;
  // console.log(to, link, testId, testDate);
  try {
    const token = jwt.sign({ email: to }, "secret_key", {
      expiresIn: "1h",
    });

    const transporter = nodemailer.createTransport({
      service: "gmail",
      auth: {
        user: process.env.GMAIL_ACCOUNT,
        pass: process.env.EMAIL_PASSWORD,
      },
    });

    const mailOptions = {
      from: process.env.GMAIL_ACCOUNT,
      to: to,
      subject: "Exam invitation!",
      html: `<h1>Dear Student,</h1><p>We have scheduled a test for you. Please click on the link below to take your test.</p><a href="${link}/?token=${token}&email=${to}">Take Test</a>`,
    };

    transporter.sendMail(mailOptions, (error, info) => {
      if (error) {
        res.status(400);
        res.send(error);
        // // console.log(error);
      } else {
        const updateQuery = {
          text: "UPDATE user_details SET invite=$1 WHERE email=$2",
          values: [true, to],
        };
        const insertQuery = {
          text: "INSERT INTO test_details (test_id, student_email, test_date) VALUES ($1, $2, $3)",
          values: [testId, to, testDate],
        };
        client.query(insertQuery, (insertErr, insertResults) => {
          if (insertErr) {
            res.status(400).send(insertErr);
          } else {
            client.query(updateQuery, (updateErr, updateResults) => {
              if (updateErr) {
                res.status(400).send(updateErr);
              } else {
                res.status(200).send({ insertResults, updateResults });
              }
            });
          }
        });
      }
    });
  } catch (e) {
    // console.log(e);
    res.send(e);
  }
});

// RESET INVITES
// app.patch("/resetinvites/", (req, res) => {
//   const query = {
//     text: "UPDATE user_details SET invite = $1",
//     values: [false],
//   };

//   client.query(query, (error, results) => {
//     if (error) {
//       res.status(400).send(error);
//     } else {
//       res.send(results).status(200);
//     }
//   });
// });

// ADD SCORE
app.patch("/studentscore/", (req, res) => {
  const { testId, email, score } = req.body;
  // // console.log(testId, email, score);
  const query = {
    text: "UPDATE test_details SET test_score=$1 WHERE test_id=$2 AND student_email=$3",
    values: [score, testId, email],
  };

  client.query(query, (error, results) => {
    if (error) {
      res.status(400).send(error);
    } else {
      res.status(200).send(results);
    }
  });
});

// GET STUDENT SCORE
app.post("/getscore/", (req, res) => {
  const { email } = req.body;

  const query = {
    text: "SELECT * FROM test_details WHERE student_email = ($1) ORDER BY test_id",
    values: [email],
  };

  client.query(query, (error, results) => {
    if (error) {
      res.status(400).send(error);
    } else {
      res.status(200).send(results.rows);
    }
  });
});

// GET PASSED COUNT
app.post("/getstudentscount/", (req, res) => {
  const query = {
    text: "SELECT COUNT(test_score) as total_passed, test_id FROM test_details WHERE test_score >= 3 GROUP BY test_id ORDER BY test_id",
  };

  client.query(query, (error, results) => {
    if (error) {
      res.send(error).status(400);
    } else {
      res.send(results).status(200);
    }
  });
});

// GET QUALIFIED COUNT
app.post("/passedtests/", (req, res) => {
  const { email } = req.body;
  const query = {
    text: "SELECT COUNT(CASE WHEN test_score > 2 THEN 1 ELSE NULL END) AS passed_count, COUNT(*) AS total_tests FROM test_Details WHERE student_email=$1",
    values: [email],
  };

  client.query(query, (error, results) => {
    if (error) {
      res.send(error).status(400);
    } else {
      // // console.log(results.rows[0]);
      res.send(results).status(200);
    }
  });
});

// INVITES COUNT
app.post("/invitescount/", (req, res) => {
  const query = {
    text: "SELECT COUNT(CASE WHEN invite = true THEN 1 ELSE NULL END) AS invites_sent, COUNT(CASE WHEN invite = false THEN 1 ELSE NULL END) AS uninvited_count FROM user_details WHERE role=$1",
    values: ["student"],
  };
  client.query(query, (error, results) => {
    if (error) {
      res.send(error).status(400);
    } else {
      // // console.log(results.rows[0]);
      res.send(results).status(200);
    }
  });
});

app.delete("/deletestudent/", (req, res) => {
  const { email } = req.query;
  // console.log(email);
  const query = {
    text: "DELETE FROM user_details WHERE email=$1",
    values: [email],
  };
  client.query(query, (error, results) => {
    if (error) {
      res.send(error).status(400);
    } else if (results.rowCount === 0) {
      // // console.log(results.rows[0]);
      res.send("User doesn't exist ").status(400);
    } else {
      res.send(results).status(200);
    }
  });
});

app.listen(PORT, () => {
  console.log(`Server is running`);
});
